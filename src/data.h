#pragma once

#include <array>
#include <vector>
#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>
#include <execution>
#include <cstdint>
#include <iostream>
#include "mpi.h"

#include "simulation.h"
#include "lbm.h"

struct Data {
    Domain domain;

    Real omega{};
    // Number of cells in a fully allocated matrix without halo.
    index_t nNodesNoHalo{};
    // Number of cells in a fully allocated matrix with halo (=one-cell envelope).
    index_t nNodesHalo{};
    Real* pop{}; // Without halo
    // A bit mask indicating which links are boundary resp. communication links.
    // TODO: should this be 64-bit ?
    bitset_t* bdLinks{}; // Without halo
    index_t bcNumLinks{}; // Total number of links that implement Dirichlet boundaries.
    pop_index_t* bcPopIndex{};
    Real* bcData{};
    // We give a halo to the density so we can compute the pseudo-potential force.
    Real* rho{}; // With halo(halo is needed for pseudo-potential force calculations).
    // This is just a temporary field for data output, not used during the algorithm.
    Real* uNorm{}; // Without halo
    // The geometry needs a halo so we can implement link-wise bounce-back on boundaries.
    NodeType* geometry{}; // With halo

    // Spans all nodes that potentially require MPI communication.
    index_t nBoundaryNodes{};
    index_t* boundaryNodeList{};

    // There is one big send (respectively receive) ommunication buffer, which 
    // is packed / unpacked in a single kernel, but communicated piecewise in
    // multiple MPI calls.
    index_t nBuffer{};
    Real* sendBuffer{};
    Real* recvBuffer{};
    // Use the two following to split the buffer for MPI communication
    std::array<index_t, 27> variablesPerDirection{};
    std::array<index_t, 27> offsetPerDirection{};

    pop_index_t* sendBufferToDataIndex{};
    pop_index_t* recvBufferToDataIndex{};

    std::array<MPI_Request, 27>  sendRequest{}, recvRequest{};

    // Returns a value in [0, 27).
    // Tells in which block (relatively speaking) the coordinate (iX, iY, iZ),
    // which is in local coordinates of a structured block without halo, is
    // located. The answer is a linear (z-major as usual) representation of a
    // blockX * blockY * blockZ grid, where all three block? values are 0 (left
    // neighbor), 1 (current block), or 2 (right neighbor).
    // Returns -1 if the coordinate is outside the computational domain or on a
    // solid node.
    int locatedOn(int iX, int iY, int iZ) const;
    void allocateFields();
    void initializeGeometry(auto const& geometryFun);
    void allocatePopulations();
    void allocateBoundaryNodeList();
    void allocateBdLinkList();
    void allocateBuffer();
    void gpuMemCheck(int taskID);
    void releaseArrays();
    void pack();
    void unpack();

    void initiateComm();
    void completeComm();

    void collideBulk();
    void collideBoundary();
    void streamCollideStreamBulk();
    void streamCollideStreamBoundary();
    void computeVelocityNorm();
    double computeLocalTotalEnergy() const;

    // Read populations from central memory at an even time step.
    void pull_even(std::array<Real, lbm::Q>& node, index_t i) const;
    // Write populations to central memory at an even time step.
    void push_even(std::array<Real, lbm::Q> const& node, index_t i);
    // Read populations from central memory at an odd time step.
    void pull_odd(std::array<Real, lbm::Q>& node, index_t i) const;
    // Write populations to central memory at an odd time step.
    void push_odd(std::array<Real, lbm::Q>& node, index_t i);

    // Compute the coordinates of a neighboring node in k-direction.
    std::tuple<int, int, int> neighbor(int iX, int iY, int iZ, int k) const;
    std::tuple<int, int, int> globalNeighbor(int iX, int iY, int iZ, int k) const;
    int computeNeighborTask(int fusedIndex) const;
};

Real computeTotalEnergy(Data const& data);
Real computeAverageEnergy(Data const& data);

inline void Data::allocateBdLinkList()
{
    bdLinks = (bitset_t*)allocateShared(nNodesNoHalo * sizeof(bitset_t));

    auto flagLinks = [=, this](const auto& iFlag)
    {
        auto i = &iFlag - bdLinks;
        bitset_t flags {};
        auto[iX, iY, iZ] = split(i, domain.ny, domain.nz);
        auto[globalX, globalY, globalZ] = domain.toGlobal(iX, iY, iZ);
        for (int k = 1; k < lbm::Q; ++k) {
            auto[nbX, nbY, nbZ] = neighbor(iX, iY, iZ, k);
            int nbLocation = locatedOn(nbX, nbY, nbZ);
            // Neighboring cell is either on a neighboring block or on a solid node.
            if (nbLocation != 13) {
                flags |= bitset_t{1U} << k;
            }
        }
        bdLinks[i] = flags;
    };

    std::for_each(policy, bdLinks, bdLinks + nNodesNoHalo, flagLinks);
}

    inline int Data::locatedOn(int iX, int iY, int iZ) const
    {
        using namespace std;
        // Add domain.n? to switch from a [-1, 0, 1] representation of
        // neighbors to a [0, 1, 2] one.
        int blockX = (iX + domain.nx) / domain.nx;
        int blockY = (iY + domain.ny) / domain.ny;
        int blockZ = (iZ + domain.nz) / domain.nz;
        auto index =  fuse(blockX, blockY, blockZ, 3, 3);
        if (index >= 27) {
            std::cout << "INSIDE LOCATEDON ==============" << std::endl;
            cout << "index = " << index << endl;
            cout << "blockX = " << blockX << endl;
            cout << "blockY = " << blockY << endl;
            cout << "blockZ = " << blockZ << endl;
            cout << "iX = " << iX << endl;
            cout << "iY = " << iY << endl;
            cout << "iZ = " << iZ << endl;
            std::cout << "=============" << std::endl;
        }
        return fuse(blockX, blockY, blockZ, 3, 3);
    }

    inline void Data::initializeGeometry(auto const& geometryFun) {
        //std::for_each(geometry, geometry + nNodesHalo,
        std::for_each(policy, geometry, geometry + nNodesHalo,
                [=, this](auto& geometryElement)
        {
            size_t iHalo = &geometryElement - geometry;
            auto[iX, iY, iZ] = split(iHalo, domain.ny + 2, domain.nz + 2);
            auto[globalX, globalY, globalZ] = domain.toGlobal(iX - 1, iY - 1, iZ - 1);
            globalX = (globalX + domain.NX) % domain.NX;
            globalY = (globalY + domain.NY) % domain.NY;
            globalZ = (globalZ + domain.NZ) % domain.NZ;
            geometryElement = geometryFun(globalX, globalY, globalZ);
        });
    }

    inline void Data::pull_even(std::array<Real, lbm::Q>& node, index_t i) const
    {
        for (int k = 0; k < lbm::Q; ++k) {
            pop_index_t popIndex = (pop_index_t)k * (pop_index_t)nNodesNoHalo + i;
            node[k] = pop[popIndex];
        }
    }

    inline void Data::push_even(std::array<Real, lbm::Q> const& node, index_t i)
    {
        for (int k = 0; k < lbm::Q; ++k) {
            pop_index_t popIndex = (pop_index_t)k * (pop_index_t)nNodesNoHalo + i;
            pop[popIndex] = node[k];
        }
    }

    inline void Data::pull_odd(std::array<Real, lbm::Q>& node, index_t i) const
    {
        auto[iX, iY, iZ] = split(i, domain.ny, domain.nz);
        auto flags = bdLinks[i];
        node[0] = pop[i];
        for (int k = 1; k < lbm::Q; ++k) {
            pop_index_t popIndexNb;
            if (flags & (bitset_t{1U} << k)) {
            popIndexNb = (pop_index_t)(lbm::Q - k) * (pop_index_t)nNodesNoHalo + i;
        }
        else {
            auto[nbX, nbY, nbZ] = neighbor(iX, iY, iZ, k);
            index_t iNb = fuse(nbX, nbY, nbZ, domain.ny, domain.nz);
            popIndexNb = (pop_index_t)k * (pop_index_t)nNodesNoHalo + iNb;
        }
        node[lbm::Q - k] = pop[popIndexNb];
    }
}

inline void Data::push_odd(std::array<Real, lbm::Q>& node, index_t i)
{
    auto[iX, iY, iZ] = split(i, domain.ny, domain.nz);
    auto flags = bdLinks[i];
    pop[i] = node[0];
    for (int k = 1; k < lbm::Q; ++k) {
        pop_index_t popIndexNb;
        if (flags & (bitset_t{1U} << k)) {
            popIndexNb = (pop_index_t)(lbm::Q - k) * (pop_index_t)nNodesNoHalo + i;
        }
        else {
            auto[nbX, nbY, nbZ] = neighbor(iX, iY, iZ, k);
            index_t iNb = fuse(nbX, nbY, nbZ, domain.ny, domain.nz);
            popIndexNb = (pop_index_t)k * (pop_index_t)nNodesNoHalo + iNb;
        }
        pop[popIndexNb] = node[lbm::Q - k];
    }
}

inline std::tuple<int, int, int> Data::neighbor(int iX, int iY, int iZ, int k) const
{
    return std::make_tuple(iX + lbm::c(k, 0), iY + lbm::c(k, 1), iZ + lbm::c(k, 2));
}

inline std::tuple<int, int, int> Data::globalNeighbor(int iX, int iY, int iZ, int k) const
{
    auto[localX, localY, localZ] = neighbor(iX, iY, iZ, k);
    auto[globalX, globalY, globalZ] = domain.toGlobal(localX, localY, localZ);
    globalX = (globalX + domain.NX) % domain.NX;
    globalY = (globalY + domain.NY) % domain.NY;
    globalZ = (globalZ + domain.NZ) % domain.NZ;
    return { globalX, globalY, globalZ };
}

inline int Data::computeNeighborTask(int fusedIndex) const {
    auto[dx, dy, dz] = split(fusedIndex, 3, 3);
    int nbTask_x = (domain.task_x + (dx - 1) + domain.blockGrid[0]) % domain.blockGrid[0];
    int nbTask_y = (domain.task_y + (dy - 1) + domain.blockGrid[1]) % domain.blockGrid[1];
    int nbTask_z = (domain.task_z + (dz - 1) + domain.blockGrid[2]) % domain.blockGrid[2];
    return fuse(nbTask_x, nbTask_y, nbTask_z, domain.blockGrid[1], domain.blockGrid[2]);
}

