#include "simulation.h"
#include "data.h"
#include "io.h"
#include "util.h"
#include "applications.h"

#include "mpi.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <cmath>

using namespace std;
using namespace std::chrono;

// const int N = 48;
const int N = 10;
const Real Re = 10.;
const Real uLB = 0.04;
const Real nuLB = uLB * (Real)N / Re;
const Real omega = 1. / (3. * nuLB + 0.5);
const int prodMaxIter = 20'000;
const int benchIniIter = 1'000;
const int benchMaxIter = 2'000;
const int outIter = 1'000;
const bool isBenchmark = true;

int main(int argc, char* argv[])
{
    int provided_guarantee = MPI_THREAD_FUNNELED;
    MPI_Init_thread(
        &argc, &argv, MPI_THREAD_FUNNELED, &provided_guarantee);

    int numTasks, taskID;
    MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &taskID);

    const int maxIter = isBenchmark ? benchMaxIter : prodMaxIter;
    const bool doOutput = isBenchmark ? false : true;

    int NX = N;
    int NY = N;
    int NZ = N;

    auto geometry { ObstacleInTheMiddle{NX, NY, NZ} };

    Domain domain(numTasks, taskID, NX, NY, NZ);
    domain.log();

    auto periodic = true;
    Data* data = new Data{domain, omega};

    if (taskID == 0) {
        if constexpr (is_same<Real, float>::value) {
            cout << "The simulation will run with single-precision arithmetics" << endl;
        }
        if constexpr (is_same<Real, double>::value) {
            cout << "The simulation will run with double-precision arithmetics" << endl;
        }
        static_assert(is_same_v<Real, float> || is_same_v<Real, double>);
        cout << "Initializing data" << endl;
    }

    data->allocateFields();
    data->initializeGeometry(geometry);
    data->allocatePopulations();
    //data->allocateBulkNodeList();
    data->allocateBoundaryNodeList();
    data->allocateBdLinkList();
    data->allocateBuffer();
    // Comment this function if running on CPUs
    data->gpuMemCheck(taskID);

    if (taskID == 0) {
        if (isBenchmark) {
            cout << "Starting warmup iterations." << endl;
        }
        else {
            cout << "Starting production iterations." << endl;
        }
    }

    auto startTime = high_resolution_clock::now();
    for (int iT = 0; iT < maxIter; iT += 2) {
        if (isBenchmark && iT == benchIniIter) {
            if (taskID == 0) {
                cout << "Starting benchmark iterations." << endl;
            }
            startTime = high_resolution_clock::now();
        }

        if (doOutput && (iT % outIter == 0)) {
            Real totalEnergy = computeTotalEnergy(*data);
            if (taskID == 0) {
                cout << "Total Energy: " << totalEnergy << endl;
            }
            data->computeVelocityNorm();

            string fname {"output"};
            string fieldName {"uNorm" + toString(iT, 6)};
            bool append = iT > 0;
            writeParallelHDF5(data->domain, data->uNorm, fname, fieldName, append);
            writeXDMF(fname, {"uNorm"}, data->domain, iT, outIter);
        }

        // EVEN TIME STEP

        // 1. Compute data on boundary and communicate the values with MPI
        data->collideBoundary();

        data->pack();
        data->initiateComm();

        // 2. Do the large part of computations in the bulk
        data->collideBulk();
        //data->applyBoundaryConditions();

        // 3. Finalize the MPI communication
        data->completeComm();
        data->unpack();
        
        // ODD TIME STEP

        // 1. Compute data on boundary and communicate the values with MPI
        data->streamCollideStreamBoundary();

        data->pack();
        data->initiateComm();

        // 2. Do the large part of computations in the bulk
        data->streamCollideStreamBulk();
        //data->applyBoundaryConditions();

        // 3. Finalize the MPI communication
        data->completeComm();
        data->unpack();
    }

    if (taskID == 0) {
        auto endTime = high_resolution_clock::now();
        auto duration = duration_cast<microseconds>(endTime - startTime);
        int measureIter = isBenchmark ? (benchMaxIter - benchIniIter) : prodMaxIter;
        double MLUPS = (double)measureIter * (double)data->domain.nNodes() / (double)duration.count();
        cout << "Elapsed: " << duration.count() * 1.e-6 << " seconds." << endl;
        if (numTasks > 1) cout << "Performance (" << numTasks << " GPUs): " << MLUPS*(double)numTasks << " MLUPS." << endl; 
        else cout << "Performance: " << MLUPS << " MLUPS." << endl;
    }

    Real averageEnergy = computeAverageEnergy(*data);

    if (taskID == 0) {
        cout << "At the end of the simulation: average energy = " << setprecision(12) << averageEnergy << endl;
    }

    data->releaseArrays();
    delete data;

    MPI_Finalize();

    return 0;
}
