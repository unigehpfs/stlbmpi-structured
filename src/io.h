#pragma once

#include "simulation.h"
#include "hdf5.h"
#include <string>
#include <vector>

void writeParallelHDF5(Domain const& domain, Real* data, std::string const& fname, std::string const& datasetName, bool append = false);

void writeXDMF(std::string fname, std::vector<std::string> fieldNames, Domain const& domain, int iT, int timeInterval, int padding = 6);
