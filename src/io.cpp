#include "globalDefs.h"
#include "simulation.h"
#include "util.h"
#include "hdf5.h"
#include "io.h"
#include "mpi.h"

#include <iostream>
#include <fstream>

using namespace std;

void writeParallelHDF5(Domain const& domain, Real* data, string const& fname, string const& datasetName, bool append)
{
    string h5_fname = fname + ".h5";
    hid_t h5_type{};
    if (sizeof(Real) == 4) {
        h5_type = H5T_NATIVE_FLOAT;
    }
    else {
        h5_type = H5T_NATIVE_DOUBLE;
    }

    // Set up file access property list with parallel I/O access.
    hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

    // Create a new file, or open an existing one, with collective access.
    hid_t file_id = append ?
        H5Fopen(h5_fname.c_str(), H5F_ACC_RDWR, plist_id) :
        H5Fcreate(h5_fname.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
    H5Pclose(plist_id);

    // Create the dataspace for the dataset.
    hsize_t full_dim[3] = { (hsize_t)domain.NX, (hsize_t)domain.NY, (hsize_t)domain.NZ };
    hid_t filespace = H5Screate_simple(3, full_dim, NULL); 

    // Create the dataset with default properties.
    hid_t dset_id = H5Dcreate(file_id, datasetName.c_str(), h5_type, filespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Sclose(filespace);

    hsize_t local_dim[3] { (hsize_t)domain.nx, (hsize_t)domain.ny, (hsize_t)domain.nz };
    hsize_t offset[3] { (hsize_t)(domain.nx * domain.task_x),
                        (hsize_t)(domain.ny * domain.task_y),
                        (hsize_t)(domain.nz * domain.task_z) };
    hid_t memspace = H5Screate_simple(3, local_dim, NULL);

    // Select hyperslab in the file.
    filespace = H5Dget_space(dset_id);
    H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL, local_dim, NULL);
    
    // Create property list for collective dataset write.
    plist_id = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);
    
    // Write the local data block into the file.
    H5Dwrite(dset_id, h5_type, memspace, filespace, plist_id, data);

    H5Dclose(dset_id);
    H5Sclose(filespace);
    H5Sclose(memspace);
    H5Pclose(plist_id);
    H5Fclose(file_id);
}

void writeXDMF(string fname, vector<string> fieldNames, Domain const& domain, int iT, int timeInterval, int padding)
{
    int mpiRank;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    if (mpiRank != 0) {
        return;
    }
    int numTimeSteps = iT / timeInterval + 1;
    ofstream ofile((fname + ".xdmf").c_str());
    ofile << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << "\n";
    ofile << "<!DOCTYPE Xdmf SYSTEM \"Xdmf.dtd\" []>" << "\n";
    ofile << "<Xdmf>" << "\n";
    ofile << "  <Domain>" << "\n";
    ofile << "    <Grid Name=\"TimeSteps\" GridType=\"Collection\" CollectionType=\"Temporal\">" << "\n";
    ofile << "    <Time TimeType=\"HyperSlab\">" << "\n";
    ofile << "        <DataItem Format=\"XML\" NumberType=\"Float\" Dimensions=\"3\">" << "\n";
    ofile << "                0 1 " << numTimeSteps << "\n";
    ofile << "        </DataItem>" << "\n";
    ofile << "    </Time>" << "\n";
    for (int iT = 0; iT < numTimeSteps; ++iT) {
        ofile << "    <Grid Name=\"Grid_" << toString(iT * timeInterval, padding) << "\">" << "\n";
        ofile << "      <Geometry Origin=\"\" Type=\"ORIGIN_DXDYDZ\">" << "\n";
        ofile << "        <DataItem DataType=\"Float\" Dimensions=\"3\" Format=\"XML\" Precision=\"" << sizeof(Real) << "\">0 0 0</DataItem>" << "\n";
        ofile << "        <DataItem DataType=\"Float\" Dimensions=\"3\" Format=\"XML\" Precision=\"" << sizeof(Real) << "\">1 1 1</DataItem>" << "\n";
        ofile << "      </Geometry>" << "\n";
        ofile << "      <Topology Dimensions=\"" << domain.NX << " " << domain.NY << " " << domain.NZ << "\" Type=\"3DCoRectMesh\"/>" << "\n";
        for (int iField = 0; iField < (int)fieldNames.size(); ++iField) {
            ofile << "      <Attribute Center=\"Cell\" Name=\"" << fieldNames[iField] << "\" Type=\"Scalar\">" << "\n";
            ofile << "        <DataItem DataType=\"Float\" Dimensions=\"" << domain.NX << " " << domain.NY << " " << domain.NZ
                  << "\" Format=\"HDF\">" << fname << ".h5:/" << fieldNames[iField] << toString(iT * timeInterval, padding)
                  << "</DataItem>" << "\n";
            ofile << "      </Attribute>" << "\n";
        }
        ofile << "    </Grid>" << "\n";
    }
    ofile << "    </Grid>" << "\n";
    ofile << "  </Domain>" << "\n";
    ofile << "</Xdmf>" << "\n";
}
