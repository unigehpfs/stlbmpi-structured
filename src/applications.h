#pragma once

#include "globalDefs.h"
#include <utility>

struct ObstacleInTheMiddle {
    int NX{}, NY{}, NZ{};
    auto operator()(int iX, int iY, int iZ) const
    {
        if ( (iX > NX / 4) &&
             (iX < 3 * NX / 4) &&
             (iY > NY / 4) &&
             (iY < 3 * NY / 4) &&
             (iZ > NZ / 4) &&
             (iZ < 3 * NZ / 4) )
        {
            return NodeType::solid;
        }
        else {
            return NodeType::fluid;
        }
    }
};

