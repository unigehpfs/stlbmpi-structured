#pragma once

#include "globalDefs.h"
#include <tuple>
#include <array>

struct Domain {
    // Total number of MPI tasks.
    int numTasks{};
    // MPI task-ID.
    int taskID{};
    // Decomposition of the MPI task-ID into a x-, y-, and z-component.
    int task_x{}, task_y{}, task_z{};
    // Dimensions of the full domain.
    int NX{}, NY{}, NZ{};
    uint64_t nNodes() const{ return (uint64_t)NX * (uint64_t)NY * (uint64_t)(NZ); }
    // Every task has one block, and all blocks have the same size.
    std::array<int, 3> blockGrid{};
    // Dimensions of a block.
    int nx{}, ny{}, nz{};

    Domain(int numTasks_, int taskID_, int NX_, int NY_, int NZ_);
    std::tuple<int, int, int> toGlobal(int iX, int iY, int iZ) const;
    std::tuple<int, int, int> toLocal(int iX, int iY, int iZ) const;
    void checkParameters() const;
    void log() const;
};

inline std::tuple<int, int, int> Domain::toGlobal(int iX, int iY, int iZ) const
{
    return { iX + task_x * nx, iY + task_y * ny, iZ + task_z * nz };
}

inline std::tuple<int, int, int> Domain::toLocal(int iX, int iY, int iZ) const
{
    return { iX - task_x * nx, iY - task_y * ny, iZ - task_z * nz };
}

// Convert a Euclidean coordinate point into a linear, z-major index.
inline index_t fuse(int iX, int iY, int iZ, int ny, int nz)
{
    return (index_t)iZ + (index_t)nz * ((index_t)iY + (index_t)ny * (index_t)iX);
}

// Convert a linear, z-major index into a Euclidean coordinate point.
inline auto split(index_t i, int ny, int nz)
{
    int iX = (int)(i / ((index_t)ny * (index_t)nz));
    index_t remainder = i % ((index_t)ny * (index_t)nz);
    int iY = (int)(remainder / (index_t)nz);
    int iZ = (int)(remainder % (index_t)nz);
    return std::make_tuple(iX, iY, iZ);
}

