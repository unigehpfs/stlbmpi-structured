#pragma once

#include <cstdlib>
#include <cstdint>
#include <execution>

#define LBM_IMPLEMENTATION "lbm-trt-unrolled-d3q19.h"
constexpr auto policy = std::execution::seq;
//constexpr auto policy = std::execution::par_unseq;

constexpr int halo_order = 1;

using Real = float;

using bitset_t = uint64_t;
// For local node indices. As long as the GPUs have less than 320 GB of memory, 32 bit is sufficient. But then, we need 64 bit.
using index_t = uint32_t;
// For local population indices. Up to 20 GB worth of populations, 32 bit is sufficient. But then, we need 64 bit.
using pop_index_t = uint64_t;

inline void* allocateShared(std::size_t numBytes) {
    void* buffer = malloc(numBytes);
    return buffer;
}

inline void releaseShared(void* buffer) {
    free(buffer);
}

inline void* allocateGPU(std::size_t numBytes) {
#ifdef USE_NVIDIA_HPC_SDK
    void* buffer;
    cudaMalloc(&buffer, numBytes);
#else
    void* buffer = malloc(numBytes);
#endif
    return buffer;
}

inline void releaseGPU(void* buffer) {
#ifdef USE_NVIDIA_HPC_SDK
    cudaFree(buffer);
#else
    free(buffer);
#endif
}

enum class NodeType : char {
    fluid = 0,
    solid = 1
};

enum class BoundaryType : char {
    bback = 0,
    vel_bback = 1,
};
