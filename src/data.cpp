#include "data.h"
#include "io.h"

#include <algorithm>
#include <execution>
#include <numeric>
#include <cmath>
#include <type_traits>
#include <ranges>

using namespace std;
using namespace std::execution;

void Data::allocateFields()
{
    nNodesNoHalo = (index_t)domain.nx * (index_t)(domain.ny) * (index_t)(domain.nz);
    nNodesHalo = (index_t)(domain.nx + 2 * halo_order) * (index_t)(domain.ny
                                     + 2 * halo_order) * (index_t)(domain.nz + 2 * halo_order);

    rho = (Real*)allocateShared(nNodesHalo * sizeof(Real));
    fill(policy, rho, rho + nNodesHalo, (Real)1);
    uNorm = (Real*)allocateShared(nNodesNoHalo * sizeof(Real));
    fill(policy, uNorm, uNorm + nNodesNoHalo, (Real)1);

    geometry = (NodeType*)allocateShared(nNodesHalo);
    fill(policy, geometry, geometry + nNodesHalo, NodeType::fluid);
}

void Data::allocatePopulations()
{
    pop = (Real*)allocateShared(nNodesNoHalo * (pop_index_t)lbm::Q * (pop_index_t)sizeof(Real));

    // Initialize populations to equilibrium with zero velocity.
    //for_each(pop, pop + nNodesNoHalo, [=, this](Real& f0)
    for_each(policy, pop, pop + nNodesNoHalo, [=, this](Real& f0)
    {
        index_t i = (index_t) (&f0 - pop);
        for (int k = 0; k < lbm::Q; ++k) {
            pop[k * lbm::Q + i] = lbm::t(k);
        }
    } );
}

void Data::allocateBoundaryNodeList()
{
    // Create a list of boundary nodes "boundaryNodeList" in 3 steps.
    // 1. Create a flag matrix which tags fluid boundary nodes with 1, and count
    //    the number of fluid boundary nodes.
    char* boundaryFlagMatrix = (char*)allocateShared(nNodesNoHalo);
    //nBoundaryNodes = transform_reduce(boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo,
    nBoundaryNodes = transform_reduce(policy, boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo,
                                            (index_t)0, plus<index_t>(), [=, this](char& boundaryFlag)
    {
        index_t iStructured = (index_t) (&boundaryFlag - boundaryFlagMatrix);
        auto[iX, iY, iZ] = split(iStructured, domain.ny, domain.nz);
        bool isBoundary = iX < halo_order || iX >= domain.nx - halo_order ||
                          iY < halo_order || iY >= domain.ny - halo_order ||
                          iZ < halo_order || iZ >= domain.nz - halo_order;
        boundaryFlag = isBoundary ? (char)1 : (char)0;
        return (index_t)boundaryFlag;
    } );

    // 2. Get a running index for boundary fluid nodes through a cumulative sum.
    index_t* boundaryCellIndex = (index_t*)allocateShared(nNodesNoHalo * sizeof(index_t));
    //exclusive_scan(boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo, boundaryCellIndex, (index_t)0); 
    exclusive_scan(policy, boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo, boundaryCellIndex, (index_t)0); 

    // 3. Parse the full domain once more to write the proper overall index
    //    into the boundary node list, at the computed running index.
    boundaryNodeList = (index_t*)allocateShared(nBoundaryNodes * sizeof(index_t));
    //for_each(boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo, [=, this](char& boundaryFlag)
    for_each(policy, boundaryFlagMatrix, boundaryFlagMatrix + nNodesNoHalo, [=, this](char& boundaryFlag)
        {
            if (boundaryFlag == (char)1) {
                index_t i = (index_t) (&boundaryFlag - boundaryFlagMatrix);
                if (i > 10'000) {
                    std::cout << "At line 75: " << std::endl;
                    std::cout << "i = " << i << std::endl;
                    std::cout << "===================" << std::endl;
                }
                boundaryNodeList[boundaryCellIndex[i]] = i;
            }
        } );

    releaseShared(boundaryCellIndex);
    releaseShared(boundaryFlagMatrix);
}

// Output of this function:
// - allocation of sendBuffer[27]
// - allocation of recvBuffer[27]
// - initialization of sendBufferToDataIndex[27]
// - initialization of recvufferToDataIndex[27]
void Data::allocateBuffer()
{
    // There is only one buffer for all directions. The data is grouped
    // direction-by-direction, so that for every direction, we can provide MPI
    // with a pointer into this buffer, at position "offsetPerDirection" and of
    // size "variablesPerDirection".

    // Compute number of variables per direction
    fill(&variablesPerDirection[0], &variablesPerDirection[0] + 27, (index_t)0);
    nBuffer = 0;
    for (index_t i = 0; i < nBoundaryNodes; ++i) {
       auto[iX, iY, iZ] = split(boundaryNodeList[i], domain.ny, domain.nz);
       for (int k = 0; k < lbm::Q; ++k) {
           auto[nbX, nbY, nbZ] = neighbor(iX, iY, iZ, k);
            if (nbX > 100) {
                std::cout << "BEFORE LOCATEDON, LINE 103,  ==============" << std::endl;
                std::cout << "boundaryNodeList[i]=" << boundaryNodeList[i] << std::endl;
                std::cout << "iX=" << iX << std::endl;
                std::cout << "nbX=" << nbX << std::endl;
                std::cout << "nbX=" << nbX << std::endl;
                std::cout << "==============" << std::endl;
            }
           int l = locatedOn(nbX, nbY, nbZ);
           if (l != 13) {
               ++variablesPerDirection[l];
               ++nBuffer;
           }
       }
    }

    // Cumulative sum gets the offset for every direction inside
    // the communication buffer.
    exclusive_scan(&variablesPerDirection[0], &variablesPerDirection[0] + 27,
            &offsetPerDirection[0], 0); 

    sendBufferToDataIndex = (pop_index_t*)allocateShared(nBuffer * sizeof(pop_index_t));
    recvBufferToDataIndex = (pop_index_t*)allocateShared(nBuffer * sizeof(pop_index_t));

    int *sendBuffer_destX = (int*)allocateShared(nBuffer * sizeof(int));
    int *sendBuffer_destY = (int*)allocateShared(nBuffer * sizeof(int));
    int *sendBuffer_destZ = (int*)allocateShared(nBuffer * sizeof(int));
    int *sendBuffer_destK = (int*)allocateShared(nBuffer * sizeof(int));
    array<index_t, 27> indexPerDirection{};
    for (index_t iBoundary = 0; iBoundary < nBoundaryNodes; ++iBoundary) {
        index_t i = boundaryNodeList[iBoundary];
        auto[iX, iY, iZ] = split(i, domain.ny, domain.nz);
        if (iX > 100) {
            std::cout << "iX=" << iX << std::endl;
            std::cout << "i=" << i << std::endl;
            std::cout << "domain.ny=" << domain.ny << std::endl;
            std::cout << "domain.nz=" << domain.nz << std::endl;
        }
        for (int k = 0; k < lbm::Q; ++k) {
            auto[nbX, nbY, nbZ] = neighbor(iX, iY, iZ, k);
            if (nbX > 100) {
                std::cout << "BEFORE LOCATEDON ==============" << std::endl;
                std::cout << "iX=" << iX << std::endl;
                std::cout << "nbX=" << nbX << std::endl;
                std::cout << "nbX=" << nbX << std::endl;
                std::cout << "==============" << std::endl;
            }
            int l = locatedOn(nbX, nbY, nbZ);
            if (l != 13) {
                pop_index_t dataIndex = (pop_index_t)(lbm::Q - k) * (pop_index_t)nNodesNoHalo + i;
                sendBufferToDataIndex[offsetPerDirection[l] + indexPerDirection[l]] = dataIndex;
                auto [destX, destY, destZ] = globalNeighbor(iX, iY, iZ, k);
                int destK = k;
                sendBuffer_destX[offsetPerDirection[l] + indexPerDirection[l]] = destX;
                sendBuffer_destY[offsetPerDirection[l] + indexPerDirection[l]] = destY;
                sendBuffer_destZ[offsetPerDirection[l] + indexPerDirection[l]] = destZ;
                sendBuffer_destK[offsetPerDirection[l] + indexPerDirection[l]] = destK;
                ++indexPerDirection[l];
            }
        }
    }

    int *recvBuffer_destX = (int*)allocateShared(nBuffer * sizeof(int));
    int *recvBuffer_destY = (int*)allocateShared(nBuffer * sizeof(int));
    int *recvBuffer_destZ = (int*)allocateShared(nBuffer * sizeof(int));
    int *recvBuffer_destK = (int*)allocateShared(nBuffer * sizeof(int));

    int tag = 0;
    for (int i = 0; i < 27; ++i) {
        if (variablesPerDirection[i] > 0) {
            int nbTask = computeNeighborTask(i);
            MPI_Status status;
            MPI_Sendrecv(sendBuffer_destX + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         recvBuffer_destX + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         MPI_COMM_WORLD, &status);
            MPI_Sendrecv(sendBuffer_destY + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         recvBuffer_destY + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         MPI_COMM_WORLD, &status);
            MPI_Sendrecv(sendBuffer_destZ + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         recvBuffer_destZ + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         MPI_COMM_WORLD, &status);
            MPI_Sendrecv(sendBuffer_destK + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         recvBuffer_destK + offsetPerDirection[i], variablesPerDirection[i], MPI_INT, nbTask, tag,
                         MPI_COMM_WORLD, &status);
        }
    }

    for (size_t iBuffer = 0; iBuffer < nBuffer; ++iBuffer) {
        auto[iX, iY, iZ] = domain.toLocal(recvBuffer_destX[iBuffer], recvBuffer_destY[iBuffer], recvBuffer_destZ[iBuffer]);
        index_t i = fuse(iX, iY, iZ, domain.ny, domain.nz);
        pop_index_t dataIndex = (pop_index_t)recvBuffer_destK[iBuffer] * (pop_index_t)nNodesNoHalo + i;
        recvBufferToDataIndex[iBuffer] = dataIndex;
    }

    releaseShared(sendBuffer_destX);
    releaseShared(sendBuffer_destY);
    releaseShared(sendBuffer_destZ);
    releaseShared(sendBuffer_destK);

    releaseShared(recvBuffer_destX);
    releaseShared(recvBuffer_destY);
    releaseShared(recvBuffer_destZ);
    releaseShared(recvBuffer_destK);

    sendBuffer = (Real*)allocateGPU(nBuffer * sizeof(Real));
    recvBuffer = (Real*)allocateGPU(nBuffer * sizeof(Real));
}

void Data::gpuMemCheck(int taskID)
{
#ifdef USE_NVIDIA_HPC_SDK
    size_t free_byte;
    size_t total_byte;
    cudaMemGetInfo(&free_byte, &total_byte);
    float free_db = (float)free_byte;
    float total_db = (float)total_byte;
    float used_db = total_db - free_db;
    std::cout << "GPU " << taskID << std::endl;
    std::cout << "GPU Memory (total) = " << total_db / 1024.0 / 1024.0 << " MB" << std::endl;
    std::cout << "GPU memory (used)  = " << used_db / 1024.0 / 1024.0 << " MB (" << 100*used_db/total_db << " %)" << std::endl;
#endif
}

void Data::releaseArrays()
{
    releaseShared(pop);
    releaseShared(rho);
    releaseShared(uNorm);
    releaseShared(geometry);
    releaseShared(boundaryNodeList);
    releaseShared(bdLinks);
    releaseShared(bcPopIndex);
    releaseShared(bcData);
    releaseGPU(sendBuffer);
    releaseGPU(recvBuffer);
    releaseShared(sendBufferToDataIndex);
    releaseShared(recvBufferToDataIndex);
}

void Data::pack()
{
    //for_each(sendBufferToDataIndex, sendBufferToDataIndex + nBuffer,
    for_each(policy, sendBufferToDataIndex, sendBufferToDataIndex + nBuffer,
            [=, this](pop_index_t const& dataIndex)
    {
        index_t bufferIndex = &dataIndex - sendBufferToDataIndex;
        sendBuffer[bufferIndex] = pop[dataIndex];
    });
}

void Data::unpack()
{
    //for_each(recvBufferToDataIndex, recvBufferToDataIndex + nBuffer,
    for_each(policy, recvBufferToDataIndex, recvBufferToDataIndex + nBuffer,
            [=, this](pop_index_t const& dataIndex)
    {
        index_t bufferIndex = &dataIndex - recvBufferToDataIndex;
        pop[dataIndex] = recvBuffer[bufferIndex];
    });
}

void Data::initiateComm()
{
    int tag = 0;
    MPI_Datatype dataType{};
    if (sizeof(Real) == 4) {
        dataType = MPI_FLOAT;
    }
    else if (sizeof(Real) == 8) {
        dataType = MPI_DOUBLE;
    }
    else {
        assert( false );
    }
    for (int i = 0; i < 27; ++i) {
        if (variablesPerDirection[i] > 0) {
            int nbTask = computeNeighborTask(i);
            MPI_Isend(sendBuffer + offsetPerDirection[i], variablesPerDirection[i], dataType, nbTask, tag, MPI_COMM_WORLD, &sendRequest[i]);
            MPI_Irecv(recvBuffer + offsetPerDirection[i], variablesPerDirection[i], dataType, nbTask, tag, MPI_COMM_WORLD, &recvRequest[i]);
        }
    }
}

void Data::completeComm()
{
    for (int i = 0; i < 27; ++i) {
        if (variablesPerDirection[i] > 0) {
            MPI_Status status;
            MPI_Wait(&sendRequest[i], &status);
            MPI_Wait(&recvRequest[i], &status);
        }
    }
}

void Data::collideBulk()
{
    size_t numBulkNodes = nNodesNoHalo - nBoundaryNodes;
    auto bulkIndices = views::iota(size_t{}, numBulkNodes);
    for_each(policy, begin(bulkIndices), end(bulkIndices),
            [=, this](auto bulkIndex)
    {
        auto [bulkX, bulkY, bulkZ] = split(bulkIndex, domain.ny - 2 * halo_order, domain.nz - 2 * halo_order);
        index_t i = fuse(bulkX + halo_order, bulkY + halo_order, bulkZ + halo_order, domain.ny, domain.nz);
        array<Real, lbm::Q> node;
        pull_even(node, i);
        lbm::collide_swap(node, omega);
        push_even(node, i);
    } );
}

void Data::collideBoundary() {
    for_each(policy, boundaryNodeList, boundaryNodeList + nBoundaryNodes,
            [=, this](index_t i)
    {
        array<Real, lbm::Q> node;
        pull_even(node, i);
        lbm::collide_swap(node, omega);
        push_even(node, i);
    } );
}

void Data::streamCollideStreamBulk() {
    size_t numBulkNodes = nNodesNoHalo - nBoundaryNodes;
    auto bulkIndices = views::iota(size_t{}, numBulkNodes);
    for_each(policy, begin(bulkIndices), end(bulkIndices),
            [=, this](auto bulkIndex)
    {
        auto [bulkX, bulkY, bulkZ] = split(bulkIndex, domain.ny - 2 * halo_order, domain.nz - 2 * halo_order);
        index_t i = fuse(bulkX + halo_order, bulkY + halo_order, bulkZ + halo_order, domain.ny, domain.nz);
        std::array<Real, lbm::Q> node;
        pull_odd(node, i);
        lbm::collide_swap(node, omega);
        push_odd(node, i);
    } );
}

void Data::streamCollideStreamBoundary()
{
    std::for_each(policy, boundaryNodeList, boundaryNodeList + nBoundaryNodes,
            [=, this](index_t i)
    {
        std::array<Real, lbm::Q> node;
        pull_odd(node, i);
        lbm::collide_swap(node, omega);
        push_odd(node, i);
    } );
}

double Data::computeLocalTotalEnergy() const
{
    //double energy = transform_reduce(pop, pop + nNodesNoHalo, 0., plus<double>(),
    double energy = transform_reduce(policy, pop, pop + nNodesNoHalo, 0., plus<double>(),
            [=, this](Real const& f0)
    {
        index_t i = (index_t)(&f0 - pop);
        array<Real, lbm::Q> node;
        pull_even(node, i);
        auto[rho, u] = lbm::macroscopic(node);
        return (double)u[0] * (double)u[0] +
               (double)u[1] * (double)u[1] +
               (double)u[2] * (double)u[2];
    } );
    return 0.5 * energy;
}

void Data::computeVelocityNorm() 
{
    //for_each(uNorm, uNorm + nNodesNoHalo,
    for_each(policy, uNorm, uNorm + nNodesNoHalo,
            [=, this](Real& uVal)
    {
        index_t i = (index_t)(&uVal - uNorm);
        auto[iX, iY, iZ] = split(i, domain.ny, domain.nz);
        index_t iHalo = fuse(iX + halo_order, iY + halo_order, iZ + halo_order,
                                       domain.ny + 2 * halo_order, domain.nz + 2 * halo_order);
        if (geometry[iHalo] == NodeType::fluid) {
            array<Real, lbm::Q> node;
            pull_even(node, i);
            auto[rho, u] = lbm::macroscopic(node);
            uVal = sqrt(u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);
        }
        else {
            uVal = (Real)0.;
        }
    } );
}

Real computeTotalEnergy(Data const& data)
{
    double energy_local = data.computeLocalTotalEnergy();
    double energy_total;
    MPI_Reduce(&energy_local, &energy_total, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    return (Real)energy_total;
}

Real computeAverageEnergy(Data const& data)
{
    auto global_nNodes = (double)data.domain.nNodes();
    return computeTotalEnergy(data) / global_nNodes;
}
