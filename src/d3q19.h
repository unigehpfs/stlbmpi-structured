#pragma once
#include <tuple>

namespace lbm {

constexpr int Q = 19;

// Translate from STLBM:
// 0  -> 1
// 1  -> 2
// 2  -> 3
// 3  -> 4
// 4  -> 5
// 5  -> 6
// 6  -> 7
// 7  -> 8
// 8  -> 9
// 9  -> 0
// 10 -> 18
// 11 -> 17
// 12 -> 16
// 13 -> 15
// 14 -> 14
// 15 -> 13
// 16 -> 12
// 17 -> 11
// 18 -> 10

constexpr int c(int iPop, int iD)
{
    constexpr int c_[Q][3] =
    {
       { 0, 0, 0}, // 0

       {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1}, // 1, 2, 3
       {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1}, // 4, 5, 6
       {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1}, // 7, 8, 9

       { 0, 1,-1}, { 0, 1, 1}, { 1, 0,-1}, // 10, 11, 12
       { 1, 0, 1}, { 1,-1, 0}, { 1, 1, 0}, // 13, 14, 15
       { 0, 0, 1}, { 0, 1, 0}, { 1, 0, 0}  // 16, 17, 18
    }; 
    return c_[iPop][iD];
}

// opp(k) = Q - k

constexpr Real t(int iPop)
{
    constexpr Real t_[Q] =
    {
        1./3.,
        1./18., 1./18., 1./18.,
        1./36., 1./36., 1./36.,
        1./36., 1./36., 1./36.,

        1./36., 1./36., 1./36.,
        1./36., 1./36., 1./36.,
        1./18., 1./18., 1./18. 
    };
    return t_[iPop];
}

auto inline macroscopic(std::array<Real, Q> const& node) {
    Real X_M1 = node[1] + node[4] + node[5] + node[6] + node[7];
    Real X_P1 = node[12] + node[13] + node[14] + node[15] + node[18];
    Real X_0  = node[0] + node[2] + node[3] + node[8] + node[9] + node[10] + node[11] + node[16] + node[17];

    Real Y_M1 = node[2] + node[4] + node[8] + node[9] + node[14];
    Real Y_P1 = node[5] + node[10] + node[11] + node[15] + node[17];

    Real Z_M1 = node[3] + node[6] + node[8] + node[10] + node[12];
    Real Z_P1 = node[7] + node[9] + node[11] + node[13] + node[16];

    Real rho = X_M1 + X_P1 + X_0;
    std::array<Real, 3> u{ (X_P1 - X_M1) / rho, (Y_P1 - Y_M1) / rho, (Z_P1 - Z_M1) / rho };
    return make_pair(rho, u);
}

}
