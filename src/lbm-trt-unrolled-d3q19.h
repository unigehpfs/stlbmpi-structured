#pragma once

#include "d3q19.h"
#include <array>

namespace lbm {

void inline collide_swap(std::array<Real, Q>& node, Real omega)
{
    auto[rho, u] = macroscopic(node);
    Real usqr_1_5 = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

    Real ck_u04 = u[0] + u[1];
    Real ck_u05 = u[0] - u[1];
    Real ck_u06 = u[0] + u[2];
    Real ck_u07 = u[0] - u[2];
    Real ck_u08 = u[1] + u[2];
    Real ck_u09 = u[1] - u[2];

    Real s_plus = omega;
    Real s_minus = 8. * (2. - s_plus) / (8. - s_plus);
    Real s1 = 1. - 0.5 * (s_plus+ s_minus);
    Real s2 = 0.5 * (-s_plus+ s_minus);
    Real s3 = 1. - s1;

    Real eq_01    = rho * t( 1) * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr_1_5);
    Real eq_02    = rho * t( 2) * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr_1_5);
    Real eq_03    = rho * t( 3) * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr_1_5);
    Real eq_04    = rho * t( 4) * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr_1_5);
    Real eq_05    = rho * t( 5) * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr_1_5);
    Real eq_06    = rho * t( 6) * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr_1_5);
    Real eq_07    = rho * t( 7) * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr_1_5);
    Real eq_08    = rho * t( 8) * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr_1_5);
    Real eq_09    = rho * t( 9) * (1. - 3. * ck_u09 + 4.5 * ck_u09 * ck_u09 - usqr_1_5);
    Real eq_00    = rho * t( 0) * (1. - usqr_1_5);
    Real eqopp_01 = eq_01 + rho * t( 1) * 6. * u[0];
    Real eqopp_02 = eq_02 + rho * t( 2) * 6. * u[1];
    Real eqopp_03 = eq_03 + rho * t( 3) * 6. * u[2];
    Real eqopp_04 = eq_04 + rho * t( 4) * 6. * ck_u04;
    Real eqopp_05 = eq_05 + rho * t( 5) * 6. * ck_u05;
    Real eqopp_06 = eq_06 + rho * t( 6) * 6. * ck_u06;
    Real eqopp_07 = eq_07 + rho * t( 7) * 6. * ck_u07;
    Real eqopp_08 = eq_08 + rho * t( 8) * 6. * ck_u08;
    Real eqopp_09 = eq_09 + rho * t( 9) * 6. * ck_u09;

    Real pop_in_01 = node[ 1];
    Real pop_in_02 = node[ 2];
    Real pop_in_03 = node[ 3];
    Real pop_in_04 = node[ 4];
    Real pop_in_05 = node[ 5];
    Real pop_in_06 = node[ 6];
    Real pop_in_07 = node[ 7];
    Real pop_in_08 = node[ 8];
    Real pop_in_09 = node[ 9];

    Real pop_in_opp_01 = node[ 18];
    Real pop_in_opp_02 = node[ 17];
    Real pop_in_opp_03 = node[ 16];
    Real pop_in_opp_04 = node[ 15];
    Real pop_in_opp_05 = node[ 14];
    Real pop_in_opp_06 = node[ 13];
    Real pop_in_opp_07 = node[ 12];
    Real pop_in_opp_08 = node[ 11];
    Real pop_in_opp_09 = node[ 10];

    node[ 18] = pop_in_01*s1 + (pop_in_opp_01 - eqopp_01)*s2 + eq_01*s3;
    node[ 17] = pop_in_02*s1 + (pop_in_opp_02 - eqopp_02)*s2 + eq_02*s3;
    node[ 16] = pop_in_03*s1 + (pop_in_opp_03 - eqopp_03)*s2 + eq_03*s3;
    node[ 15] = pop_in_04*s1 + (pop_in_opp_04 - eqopp_04)*s2 + eq_04*s3;
    node[ 14] = pop_in_05*s1 + (pop_in_opp_05 - eqopp_05)*s2 + eq_05*s3;
    node[ 13] = pop_in_06*s1 + (pop_in_opp_06 - eqopp_06)*s2 + eq_06*s3;
    node[ 12] = pop_in_07*s1 + (pop_in_opp_07 - eqopp_07)*s2 + eq_07*s3;
    node[ 11] = pop_in_08*s1 + (pop_in_opp_08 - eqopp_08)*s2 + eq_08*s3;
    node[ 10] = pop_in_09*s1 + (pop_in_opp_09 - eqopp_09)*s2 + eq_09*s3;

    node[  1] = pop_in_opp_01*s1 + (pop_in_01 - eq_01)*s2 + eqopp_01*s3;
    node[  2] = pop_in_opp_02*s1 + (pop_in_02 - eq_02)*s2 + eqopp_02*s3;
    node[  3] = pop_in_opp_03*s1 + (pop_in_03 - eq_03)*s2 + eqopp_03*s3;
    node[  4] = pop_in_opp_04*s1 + (pop_in_04 - eq_04)*s2 + eqopp_04*s3;
    node[  5] = pop_in_opp_05*s1 + (pop_in_05 - eq_05)*s2 + eqopp_05*s3;
    node[  6] = pop_in_opp_06*s1 + (pop_in_06 - eq_06)*s2 + eqopp_06*s3;
    node[  7] = pop_in_opp_07*s1 + (pop_in_07 - eq_07)*s2 + eqopp_07*s3;
    node[  8] = pop_in_opp_08*s1 + (pop_in_08 - eq_08)*s2 + eqopp_08*s3;
    node[  9] = pop_in_opp_09*s1 + (pop_in_09 - eq_09)*s2 + eqopp_09*s3;

    node[  0] = (1. - omega) * node[ 0] + omega * eq_00;
}

}
