#pragma once

#include <array>

#include "d3q19.h"

namespace lbm {

void inline collide_swap(std::array<Real, Q>& node, Real omega)
{
    auto[rho, u] = macroscopic(node);
    Real usqr_1_5 = 1.5 * (u[0] * u[0] + u[1] * u[1] + u[2] * u[2]);

    Real ck_u04 = u[0] + u[1];
    Real ck_u05 = u[0] - u[1];
    Real ck_u06 = u[0] + u[2];
    Real ck_u07 = u[0] - u[2];
    Real ck_u08 = u[1] + u[2];
    Real ck_u09 = u[1] - u[2];

    Real eq_01    = rho * t( 1) * (1. - 3. * u[0] + 4.5 * u[0] * u[0] - usqr_1_5);
    Real eq_02    = rho * t( 2) * (1. - 3. * u[1] + 4.5 * u[1] * u[1] - usqr_1_5);
    Real eq_03    = rho * t( 3) * (1. - 3. * u[2] + 4.5 * u[2] * u[2] - usqr_1_5);
    Real eq_04    = rho * t( 4) * (1. - 3. * ck_u04 + 4.5 * ck_u04 * ck_u04 - usqr_1_5);
    Real eq_05    = rho * t( 5) * (1. - 3. * ck_u05 + 4.5 * ck_u05 * ck_u05 - usqr_1_5);
    Real eq_06    = rho * t( 6) * (1. - 3. * ck_u06 + 4.5 * ck_u06 * ck_u06 - usqr_1_5);
    Real eq_07    = rho * t( 7) * (1. - 3. * ck_u07 + 4.5 * ck_u07 * ck_u07 - usqr_1_5);
    Real eq_08    = rho * t( 8) * (1. - 3. * ck_u08 + 4.5 * ck_u08 * ck_u08 - usqr_1_5);
    Real eq_09    = rho * t( 9) * (1. - 3. * ck_u09 + 4.5 * ck_u09 * ck_u09 - usqr_1_5);
    Real eq_00    = rho * t( 0) * (1. - usqr_1_5);
    Real eqopp_01 = eq_01 + rho * t( 1) * 6. * u[0];
    Real eqopp_02 = eq_02 + rho * t( 2) * 6. * u[1];
    Real eqopp_03 = eq_03 + rho * t( 3) * 6. * u[2];
    Real eqopp_04 = eq_04 + rho * t( 4) * 6. * ck_u04;
    Real eqopp_05 = eq_05 + rho * t( 5) * 6. * ck_u05;
    Real eqopp_06 = eq_06 + rho * t( 6) * 6. * ck_u06;
    Real eqopp_07 = eq_07 + rho * t( 7) * 6. * ck_u07;
    Real eqopp_08 = eq_08 + rho * t( 8) * 6. * ck_u08;
    Real eqopp_09 = eq_09 + rho * t( 9) * 6. * ck_u09;

    Real pop_out_01 = (1. - omega) * node[  1] + omega * eq_01;
    Real pop_out_18 = (1. - omega) * node[ 18] + omega * eqopp_01;
    node[  1] = pop_out_18;
    node[ 18] = pop_out_01;

    Real pop_out_02 = (1. - omega) * node[  2] + omega * eq_02;
    Real pop_out_17 = (1. - omega) * node[ 17] + omega * eqopp_02;
    node[  2] = pop_out_17;
    node[ 17] = pop_out_02;

    Real pop_out_03 = (1. - omega) * node[  3] + omega * eq_03;
    Real pop_out_16 = (1. - omega) * node[ 16] + omega * eqopp_03;
    node[  3] = pop_out_16;
    node[ 16] = pop_out_03;

    Real pop_out_04 = (1. - omega) * node[  4] + omega * eq_04;
    Real pop_out_15 = (1. - omega) * node[ 15] + omega * eqopp_04;
    node[  4] = pop_out_15;
    node[ 15] = pop_out_04;

    Real pop_out_05 = (1. - omega) * node[  5] + omega * eq_05;
    Real pop_out_14 = (1. - omega) * node[ 14] + omega * eqopp_05;
    node[  5] = pop_out_14;
    node[ 14] = pop_out_05;

    Real pop_out_06 = (1. - omega) * node[  6] + omega * eq_06;
    Real pop_out_13 = (1. - omega) * node[ 13] + omega * eqopp_06;
    node[  6] = pop_out_13;
    node[ 13] = pop_out_06;

    Real pop_out_07 = (1. - omega) * node[  7] + omega * eq_07;
    Real pop_out_12 = (1. - omega) * node[ 12] + omega * eqopp_07;
    node[  7] = pop_out_12;
    node[ 12] = pop_out_07;

    Real pop_out_08 = (1. - omega) * node[  8] + omega * eq_08;
    Real pop_out_11 = (1. - omega) * node[ 11] + omega * eqopp_08;
    node[  8] = pop_out_11;
    node[ 11] = pop_out_08;

    Real pop_out_09 = (1. - omega) * node[  9] + omega * eq_09;
    Real pop_out_10 = (1. - omega) * node[ 10] + omega * eqopp_09;
    node[  9] = pop_out_10;
    node[ 10] = pop_out_09;

    node[ 0] = (1. - omega) * node[ 0] + omega * eq_00;
}

}
