#pragma once

#include <array>

#include "d3q19.h"

namespace lbm {

auto inline collide(std::array<Real, Q> const& fin, int k, Real rho, std::array<Real, 3> const& u, Real usqr, Real omega)
{
    Real ck_u = c(k, 0) * u[0] + c(k, 1) * u[1] + c(k, 2) * u[2];
    Real eq = rho * t(k) * (1. + 3. * ck_u + 4.5 * ck_u * ck_u - 1.5 * usqr);
    Real eqopp = eq - 6.* rho * t(k) * ck_u;
    Real pop_out = (1. - omega) * fin[k] + omega * eq;
    Real pop_out_opp = (1. - omega) * fin[Q - k] + omega * eqopp;
    return std::make_pair(pop_out, pop_out_opp);
}

void inline collide_swap(std::array<Real, Q>& node, Real omega)
{
    auto[rho, u] = macroscopic(node);
    Real usqr = u[0] * u[0] + u[1] * u[1] + u[2] * u[2];

    for (int k = 1; k <= Q / 2; ++k) {
        auto[pop_out, pop_out_opp] = collide(node, k, rho, u, usqr, omega);
        // Invert them to implement the swap operation.
        node[Q - k] = pop_out;
        node[k] = pop_out_opp;
    }

    for (int k: {0}) {
        Real eq = rho * t(k) * (1. - 1.5 * usqr);
        node[k] =  (1. - omega) * node[k] + omega * eq;
    }
}

}
