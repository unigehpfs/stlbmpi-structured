#include "loadBalancing.h"

#include <numeric>
#include <cassert>
#include <limits>

using namespace std;

vector<int> factorize(int value)
{
    std::vector<int> primeFactors;
    int testFactor = 2;
    while (testFactor <= value) {
        if (value % testFactor == 0) {
            value /= testFactor;
            primeFactors.push_back(testFactor);
        }
        else {
            ++testFactor;
        }
    }
    return primeFactors;
}

static double computeDisproportion(array<int, 3> const& attempt, int nx, int ny, int nz)
{
    double ratio0 = (double)attempt[0] / (double)nx;
    double ratio1 = (double)attempt[1] / (double)ny;
    double ratio2 = (double)attempt[2] / (double)nz;
    
    double disprop01 = ratio0 > ratio1 ? ratio0 / ratio1 : ratio1 / ratio0;
    double disprop02 = ratio0 > ratio2 ? ratio0 / ratio2 : ratio2 / ratio0;
    double disprop12 = ratio1 > ratio2 ? ratio1 / ratio2 : ratio2 / ratio1;

    return max(max(disprop01, disprop02), disprop12);
}

static array<int, 3> proposeBalance(vector<int> const& factors, vector<int> const& attribution) {
    assert(factors.size() == attribution.size());
    array<int, 3> solution{ 1, 1, 1};
    for (int i = 0; i < (int)factors.size(); ++i) {
        assert(attribution[i] >= 0 && attribution[i] < 3);
        solution[attribution[i]] *= factors[i];
    }
    return solution;
}

array<int, 3> balance(int numTasks, int nx, int ny, int nz)
{
    array<int, 3> solution{ 0, 0, 0};
    if (numTasks <= 0) return solution;
    vector<int> factors = factorize(numTasks);
    int numFactors = (int)factors.size();
    vector<int> attribution(numFactors, 0);
    double previousDisproportion = numeric_limits<double>::max();
    while(true) {
        array<int, 3> attempt = proposeBalance(factors, attribution);
        double disproportion = computeDisproportion(attempt, nx, ny, nz);
        if (disproportion < previousDisproportion) {
            previousDisproportion = disproportion;
            solution = attempt;
        }

        int i = 0;
        while (i < numFactors && attribution[i] == 2) {
            attribution[i] = 0;
            ++i;
        }
        if (i == numFactors) break;
        ++attribution[i];
    }
    return solution;
}
