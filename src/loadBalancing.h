#pragma once

#include <array>
#include <vector>

// Decompose a value into prime factors
std::vector<int> factorize(int value);

// Decompose the number of tasks into a product nx * ny * nz
// as evenly as possible. The algorithm is naive and has very bad
// complexity, but for a reasonable amount of tasks (~10'000) it
// should still be done in a matter of seconds.
std::array<int, 3> balance(int numTasks, int nx, int ny, int nz);

