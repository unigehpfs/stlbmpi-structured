#include "simulation.h"
#include "loadBalancing.h"
#include <iostream>
#include <stdexcept>

using namespace std;

Domain::Domain(int numTasks_, int taskID_, int NX_, int NY_, int NZ_)
    : numTasks(numTasks_),
      taskID(taskID_),
      NX(NX_),
      NY(NY_),
      NZ(NZ_),
      blockGrid( balance(numTasks, NX, NY, NZ) ),
      nx(NX / blockGrid[0]),
      ny(NY / blockGrid[1]),
      nz(NZ / blockGrid[2])
      
{
    tie(task_x, task_y, task_z) = split(taskID, blockGrid[1], blockGrid[2]);
    checkParameters();
}

void Domain::log() const
{
    cout << "Domain: " << nx << " x " << ny << " x " << nz
         << ", Grid: " << blockGrid[0] << " x " << blockGrid[1] << " x " << blockGrid[2] << endl;
}

void Domain::checkParameters() const
{
    if (NX % blockGrid[0] != 0) {
        throw invalid_argument("X dimension is not divisible by number of blocks in x-direction.");
    }
    if (NY % blockGrid[1] != 0) {
        throw invalid_argument("Y dimension is not divisible by number of blocks in y-direction.");
    }
    if (NZ % blockGrid[2] != 0) {
        throw invalid_argument("Z dimension is not divisible by number of blocks in z-direction.");
    }
}

