#include "util.h"
#include <sstream>
#include <iomanip>

using namespace std;

string toString(int value, int width) {
    stringstream sstream;
    sstream << setw(width) << setfill('0') << value;
    return sstream.str();
}
