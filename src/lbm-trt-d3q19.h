#pragma once

#include "d3q19.h"
#include <array>

namespace lbm {

auto inline collide(std::array<Real, Q> const& fin, int k, Real rho, std::array<Real, 3> const& u, Real usqr, Real omega)
{
    Real s_plus = omega;
    Real s_minus = 8. * (2. - s_plus) / (8. - s_plus);
    Real ck_u = c(k, 0) * u[0] + c(k, 1) * u[1] + c(k, 2) * u[2];
    
    Real eq_plus = rho * t(k) * (1. + 4.5 * ck_u * ck_u - 1.5 * usqr);
    Real eq_minus = rho * t(k) * 3. * ck_u;
    Real pop_in = fin[k];
    Real pop_in_opp = fin[Q - k];
    Real f_plus = 0.5 * (pop_in + pop_in_opp);
    Real f_minus = f_plus - pop_in_opp;

    Real pop_out = pop_in - s_plus * (f_plus- eq_plus) - s_minus * (f_minus - eq_minus);
    Real pop_out_opp = pop_in_opp - s_plus * (f_plus - eq_plus) + s_minus * (f_minus - eq_minus);

    return std::make_pair(pop_out, pop_out_opp);
}

void inline collide_swap(std::array<Real, Q>& node, Real omega)
{
    auto[rho, u] = macroscopic(node);
    Real usqr = u[0] * u[0] + u[1] * u[1] + u[2] * u[2];

    for (int k = 1; k <= Q / 2; ++k) {
        auto[pop_out, pop_out_opp] = collide(node, k, rho, u, usqr, omega);
        // Invert them to implement the swap operation.
        node[Q - k] = pop_out;
        node[k] = pop_out_opp;
    }

    for (int k: {0}) {
        Real eq = rho * t(k) * (1. - 1.5 * usqr);
        node[k] =  (1. - omega) * node[k] + omega * eq;
    }
}

}
